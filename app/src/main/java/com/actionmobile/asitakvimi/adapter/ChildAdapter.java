package com.actionmobile.asitakvimi.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.actionmobile.asitakvimi.R;
import com.actionmobile.asitakvimi.activity.VaccineActivity;
import com.actionmobile.asitakvimi.model.Child;
import com.actionmobile.asitakvimi.model.Vaccine.AsilarBean;
import com.actionmobile.asitakvimi.util.Constance;
import com.bumptech.glide.Glide;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;

public class ChildAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final List<Child> childList = new ArrayList<>();
    private HashMap<Integer,AsilarBean> asilarHashMap = new HashMap<>();
    private LayoutInflater mInflater;
    private Context context;

    public ChildAdapter(Context context, HashMap<Integer,AsilarBean> asilarHashMap) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
        this.asilarHashMap = asilarHashMap;
    }

    public void setList(List<Child> childList) {
        this.childList.clear();
        this.childList.addAll(childList);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_child, parent, false);
        return new ChildViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ChildViewHolder childViewHolder = (ChildViewHolder) holder;
        Child child = childList.get(position);
        if (child.picturePath != null) {
            File imageFile = new File(child.picturePath);
            if (imageFile.exists()) {
                Bitmap compressedBitmap = null;
                try {
                    compressedBitmap = new Compressor(context)
                            .setMaxWidth(150)
                            .setMaxHeight(150)
                            .compressToBitmap(imageFile);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (compressedBitmap != null) {
                    Glide.with(context)
                            .load(compressedBitmap)
                            .into(childViewHolder.imgChild);
                }

            } else {
                childViewHolder.imgChild.setImageResource(R.drawable.ic_child);
            }

        } else {
            childViewHolder.imgChild.setImageResource(R.drawable.ic_child);
        }

        childViewHolder.tvName.setText(child.name);
        childViewHolder.tvDate.setText(child.birthDate);
        childViewHolder.itemView.setOnClickListener(view -> {
            Intent intent = new Intent(context, VaccineActivity.class);
            intent.putExtra(Constance.KEY_ACTIVITY, child.id);
            intent.putExtra(Constance.KEY_ASI, asilarHashMap);
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return childList.size();
    }


    public class ChildViewHolder extends RecyclerView.ViewHolder {
        CircleImageView imgChild;
        TextView tvName;
        TextView tvDate;

        public ChildViewHolder(View itemView) {
            super(itemView);
            imgChild = itemView.findViewById(R.id.img_child);
            tvName = itemView.findViewById(R.id.tv_name);
            tvDate = itemView.findViewById(R.id.tv_date);
        }
    }
}
