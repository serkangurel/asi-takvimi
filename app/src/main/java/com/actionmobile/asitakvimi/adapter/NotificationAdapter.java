package com.actionmobile.asitakvimi.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.actionmobile.asitakvimi.R;
import com.actionmobile.asitakvimi.activity.VaccineDetailActivity;
import com.actionmobile.asitakvimi.model.Child;
import com.actionmobile.asitakvimi.model.Notifications;
import com.actionmobile.asitakvimi.model.Vaccine.AsilarBean;
import com.actionmobile.asitakvimi.util.Constance;
import com.actionmobile.asitakvimi.util.Tools;
import com.bumptech.glide.Glide;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;

public class NotificationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final List<Notifications> notificationList = new ArrayList<>();
    private HashMap<Integer,AsilarBean> asilarHashMap;
    private LayoutInflater mInflater;
    private Context context;

    public NotificationAdapter(Context context, HashMap<Integer,AsilarBean> asilarHashMap) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
        this.asilarHashMap = asilarHashMap;
    }

    public void setList(List<Notifications> notificationList) {
        this.notificationList.clear();
        this.notificationList.addAll(notificationList);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_notification, parent, false);
        return new NotificationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        NotificationViewHolder notificationHolder = (NotificationViewHolder) holder;
        Notifications notification = notificationList.get(position);
        long date = Tools.getInstance().getMillisecondFromDate(notification.takvim.Tarih);
        String vaccineName = asilarHashMap.get(notification.takvim.AsiId).KisaAdi;
        updateNotificationMessage(date, vaccineName, notification);

        Child child = notification.child;
        if (child.picturePath != null) {
            File imageFile = new File(child.picturePath);
            if (imageFile.exists()) {
                Bitmap compressedBitmap = null;
                try {
                    compressedBitmap = new Compressor(context)
                            .setMaxWidth(150)
                            .setMaxHeight(150)
                            .compressToBitmap(imageFile);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (compressedBitmap != null) {
                    Glide.with(context)
                            .load(compressedBitmap)
                            .into(notificationHolder.imgChild);
                }

            } else {
                notificationHolder.imgChild.setImageResource(R.drawable.ic_child);
            }

        } else {
            notificationHolder.imgChild.setImageResource(R.drawable.ic_child);
        }

        notificationHolder.tvName.setText(child.name);
        notificationHolder.tvMessage.setText(notification.message);
        notificationHolder.itemView.setOnClickListener(view -> {
            Intent intent = new Intent(context, VaccineDetailActivity.class);
            intent.putExtra(Constance.KEY_CHILD, child.id);
            intent.putExtra(Constance.KEY_TAKVIM, notification.takvim);
            intent.putExtra(Constance.KEY_ASI, asilarHashMap.get(notification.takvim.AsiId));
            context.startActivity(intent);
        });

    }

    private void updateNotificationMessage(long date, String vaccineName, Notifications notification) {
        if (!DateUtils.isToday(date)) {
            notification.message = new StringBuilder()
                    .append("Geçmiş tarihli bir aşınız var: ")
                    .append(vaccineName)
                    .append(" - ")
                    .append(notification.takvim.Tarih).toString();
        } else {
            notification.message = new StringBuilder()
                    .append("Aşı zamanı geldi: ")
                    .append(vaccineName)
                    .append(" - ")
                    .append(notification.takvim.Tarih).toString();
        }
    }

    @Override
    public int getItemCount() {
        return notificationList.size();
    }


    public class NotificationViewHolder extends RecyclerView.ViewHolder {
        CircleImageView imgChild;
        TextView tvName;
        TextView tvMessage;

        public NotificationViewHolder(View itemView) {
            super(itemView);
            imgChild = itemView.findViewById(R.id.img_child);
            tvName = itemView.findViewById(R.id.tv_child_name);
            tvMessage = itemView.findViewById(R.id.tv_notification);
        }
    }
}
