package com.actionmobile.asitakvimi.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionmobile.asitakvimi.R;
import com.actionmobile.asitakvimi.activity.VaccineDetailActivity;
import com.actionmobile.asitakvimi.model.Child;
import com.actionmobile.asitakvimi.model.Vaccine.AsilarBean;
import com.actionmobile.asitakvimi.model.Vaccine.TakvimBean;
import com.actionmobile.asitakvimi.util.Constance;
import com.actionmobile.asitakvimi.util.Tools;

import java.util.ArrayList;
import java.util.HashMap;

public class VaccineAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private LayoutInflater mInflater;
    private Context context;
    private Child child;
    private HashMap<Integer,AsilarBean> asilarHashMap;
    private final ArrayList<TakvimBean> takvimList = new ArrayList<>();


    public VaccineAdapter(Context context, HashMap<Integer,AsilarBean> asilarHashMap, Child child) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
        this.asilarHashMap = asilarHashMap;
        this.child = child;
    }

    public void setList(ArrayList<TakvimBean> takvimList) {
        this.takvimList.clear();
        this.takvimList.addAll(takvimList);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_vaccine, parent, false);
        return new VaccineViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        VaccineViewHolder vaccineHolder = (VaccineViewHolder) holder;
        TakvimBean takvimBean = takvimList.get(position);
        setImage(vaccineHolder.imgVaccine, takvimBean);
        vaccineHolder.tvVaccineName.setText(asilarHashMap.get(takvimBean.AsiId).KisaAdi);
        vaccineHolder.tvVaccineDate.setText(takvimBean.Tarih);
        vaccineHolder.itemView.setOnClickListener(view -> {
            Intent intent = new Intent(context, VaccineDetailActivity.class);
            intent.putExtra(Constance.KEY_CHILD, child.id);
            intent.putExtra(Constance.KEY_TAKVIM, takvimBean);
            intent.putExtra(Constance.KEY_ASI, asilarHashMap.get(takvimBean.AsiId));
            context.startActivity(intent);
        });
    }

    private void setImage(ImageView imgVaccine, TakvimBean takvimBean) {

        boolean isDateFromPast = Tools.getInstance().isDateFromPast(takvimBean.Tarih);

        if (isDateFromPast) {
            if (takvimBean.Durum) {
                imgVaccine.setImageResource(R.drawable.ic_checked);
            } else {
                imgVaccine.setImageResource(R.drawable.ic_error);
            }
        } else {
            imgVaccine.setImageResource(R.drawable.ic_locked);
        }
    }

    @Override
    public int getItemCount() {
        return takvimList.size();
    }

    private class VaccineViewHolder extends RecyclerView.ViewHolder {
        ImageView imgVaccine;
        TextView tvVaccineName;
        TextView tvVaccineDate;

        public VaccineViewHolder(View view) {
            super(view);
            imgVaccine = view.findViewById(R.id.img_vaccine);
            tvVaccineName = view.findViewById(R.id.tv_vaccine_name);
            tvVaccineDate = view.findViewById(R.id.tv_vaccine_date);

        }
    }
}