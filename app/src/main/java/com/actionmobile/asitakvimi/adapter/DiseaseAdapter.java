package com.actionmobile.asitakvimi.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionmobile.asitakvimi.R;
import com.actionmobile.asitakvimi.activity.DiseaseDetailActivity;
import com.actionmobile.asitakvimi.model.Vaccine.HastaliklarBean;
import com.actionmobile.asitakvimi.util.Constance;

import java.util.ArrayList;
import java.util.List;

public class DiseaseAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final List<HastaliklarBean> diseaseList = new ArrayList<>();
    private LayoutInflater mInflater;
    private Context context;

    public DiseaseAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
    }

    public void setList(List<HastaliklarBean> diseaseList) {
        this.diseaseList.clear();
        this.diseaseList.addAll(diseaseList);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_diseases, parent, false);
        return new DiseaseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        DiseaseViewHolder diseaseViewHolder = (DiseaseViewHolder) holder;
        diseaseViewHolder.imgChild.setImageResource(R.drawable.ic_healthcare);
        diseaseViewHolder.tvName.setText(diseaseList.get(position).HastalikAdi);
        diseaseViewHolder.itemView.setOnClickListener(view -> {
            Intent intent = new Intent(context, DiseaseDetailActivity.class);
            intent.putExtra(Constance.KEY_ACTIVITY, diseaseList.get(position));
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return diseaseList.size();
    }

    public class DiseaseViewHolder extends RecyclerView.ViewHolder {
        ImageView imgChild;
        TextView tvName;

        public DiseaseViewHolder(View itemView) {
            super(itemView);
            imgChild = itemView.findViewById(R.id.img_diseases);
            tvName = itemView.findViewById(R.id.tv_diseases);
        }
    }
}
