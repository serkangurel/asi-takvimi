package com.actionmobile.asitakvimi.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.actionmobile.asitakvimi.R;
import com.actionmobile.asitakvimi.fragments.AboutVaccineFragment;
import com.actionmobile.asitakvimi.fragments.ChildsFragment;
import com.actionmobile.asitakvimi.fragments.DiseasesFragment;
import com.actionmobile.asitakvimi.fragments.NotificationFragment;
import com.actionmobile.asitakvimi.model.Vaccine.AsilarBean;
import com.actionmobile.asitakvimi.model.Vaccine.HastaliklarBean;
import com.actionmobile.asitakvimi.model.Vaccine.TakvimBean;
import com.actionmobile.asitakvimi.util.Tools;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SectionsPagerAdapter extends FragmentPagerAdapter {
    private String asilarHakkinda;
    private Context context;
    private ArrayList<HastaliklarBean> hastaliklarList = new ArrayList<>();
    private HashMap<Integer,AsilarBean> asilarHashMap = new HashMap<>();
    private ArrayList<TakvimBean> takvimList = new ArrayList<>();
    private int pageCount = 4;

    public SectionsPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
        init();
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return ChildsFragment.newInstance(takvimList, asilarHashMap);
            case 1:
                return NotificationFragment.newInstance(asilarHashMap, "");
            case 2:
                return AboutVaccineFragment.newInstance(asilarHakkinda, "");
            case 3:
                return DiseasesFragment.newInstance(hastaliklarList, "");
        }
        return null;
    }

    @Override
    public int getCount() {
        return pageCount;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return context.getString(R.string.childs);
            case 1:
                return context.getString(R.string.notifications);
            case 2:
                return context.getString(R.string.about_vaccine);
            case 3:
                return context.getString(R.string.preventable_diseases);
        }
        return null;
    }

    private void init() {
        String json = Tools.getInstance().loadJSONFromAsset(context, "Asi.json");
        try {
            JSONObject jsonObject = new JSONObject(json);
            asilarHakkinda = jsonObject.getString("AsilarHakkinda");

            JSONArray hastaliklarArray = jsonObject.getJSONArray("Hastaliklar");
            Type typeHastaliklar = new TypeToken<List<HastaliklarBean>>() {
            }.getType();
            hastaliklarList = new Gson().fromJson(hastaliklarArray.toString(), typeHastaliklar);

            JSONArray asilarArray = jsonObject.getJSONArray("Asilar");
            JSONArray takvimArray = jsonObject.getJSONArray("Takvim");

            Type typeAsilar = new TypeToken<List<AsilarBean>>() {
            }.getType();

            Type typeTakvim = new TypeToken<List<TakvimBean>>() {
            }.getType();

            ArrayList<AsilarBean> asilarArrayList = new Gson().fromJson(asilarArray.toString(), typeAsilar);
            takvimList = new Gson().fromJson(takvimArray.toString(), typeTakvim);

            for (AsilarBean asilarBean : asilarArrayList) {
                asilarHashMap.put(asilarBean.id, asilarBean);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}
