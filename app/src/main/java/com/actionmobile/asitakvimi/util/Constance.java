package com.actionmobile.asitakvimi.util;

public class Constance {

    public static final String ARG_PARAM1 = "param1";

    public static final String ARG_PARAM2 = "param2";

    public static final String HAWK_CHILDS = "hawk_childs";

    public static final String HAWK_NOTIFICATIONS = "hawk_notifications";

    public static final String HAWK_REMOVED_NOTIFICATIONS = "hawk_removed_notification";

    public static final String KEY_ACTIVITY = "activity";

    public static final String KEY_TAKVIM = "takvim";

    public static final String KEY_ASI = "asi";

    public static final String KEY_CHILD = "child";

    public static final String KEY_NOTIFICATION = "notification";
}
