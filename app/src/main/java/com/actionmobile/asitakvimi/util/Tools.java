package com.actionmobile.asitakvimi.util;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.actionmobile.asitakvimi.BuildConfig;
import com.actionmobile.asitakvimi.R;
import com.actionmobile.asitakvimi.model.Child;
import com.actionmobile.asitakvimi.model.Vaccine.TakvimBean;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class Tools {

    private static final Tools ourInstance = new Tools();

    public static Tools getInstance() {
        return ourInstance;
    }

    private Tools() {
    }

    public String loadJSONFromAsset(Context context, String fileName) {
        String json = null;
        try {
            InputStream is = context.getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public void setNotification(Context context, TakvimBean takvimBean, Child child, String vaccineName, long date) {

        Notification notification = getNotification(context, child.name, vaccineName);

        Intent notificationIntent = new Intent(context, NotificationPublisher.class);
        Bundle bundle = new Bundle();
        bundle.putInt(NotificationPublisher.NOTIFICATION_ID, 1);
        bundle.putSerializable(NotificationPublisher.KEY_CHILD, child);
        bundle.putParcelable(NotificationPublisher.NOTIFICATION, notification);
        bundle.putSerializable(NotificationPublisher.KEY_TAKVIM, takvimBean);
        notificationIntent.putExtra(NotificationPublisher.KEY_BUNDLE, bundle);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, takvimBean.BildirimId, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        if (alarmManager != null) {
            alarmManager.set(AlarmManager.RTC_WAKEUP, date, pendingIntent);
        }
    }

    public void cancelAlarm(Context context, int id) {
        Intent notificationIntent = new Intent(context, NotificationPublisher.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, id, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        if (alarmManager != null) {
            alarmManager.cancel(pendingIntent);
        }
    }

    private Notification getNotification(Context context, String childName, String vaccineName) {
        NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
        bigText.bigText(new StringBuilder()
                .append("Çocuğunuzun (")
                .append(childName)
                .append(") aşı zamanı geldi: ")
                .append(vaccineName)
                .append(". Lütfen doktorunuza danışınız!").toString());

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, NotificationPublisher.CHANNEL_ID)
                .setStyle(bigText)
                .setContentTitle("Aşı zamanı")
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setSmallIcon(R.drawable.ic_medicine)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
        return builder.build();
    }

    public boolean isDateFromPast(String date) {
        long dateMillis = getMillisecondFromDate(date);
        long currentTimeMillis = System.currentTimeMillis();
        return currentTimeMillis - dateMillis > 0;
    }

    public float dpFromPx(final Context context, final float px) {
        return px / context.getResources().getDisplayMetrics().density;
    }

    public float pxFromDp(final Context context, final float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }

    //-90 derece
    public Bitmap rotateBitmap(String path, int degrees) {
        Bitmap bmpOriginal = BitmapFactory.decodeFile(path);
        if (bmpOriginal == null) {
            return null;
        }
        Bitmap bmResult = Bitmap.createBitmap(bmpOriginal.getWidth(), bmpOriginal.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas tempCanvas = new Canvas(bmResult);
        tempCanvas.rotate(degrees, bmpOriginal.getWidth() / 2, bmpOriginal.getHeight() / 2);
        tempCanvas.drawBitmap(bmpOriginal, 0, 0, null);
        return bmResult;


    }


    public Bitmap rotateBitmap(String path) {
        Bitmap myBitmap = BitmapFactory.decodeFile(path);
        if (myBitmap == null) {
            return null;
        }
        try {
            ExifInterface exif = new ExifInterface(path);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
            } else if (orientation == 3) {
                matrix.postRotate(180);
            } else if (orientation == 8) {
                matrix.postRotate(270);
            }
            myBitmap = Bitmap.createBitmap(myBitmap, 0, 0, myBitmap.getWidth(), myBitmap.getHeight(), matrix, true); // rotating bitmap
        } catch (Exception e) {
            e.printStackTrace();
        }
        return myBitmap;
    }


    public String getDownloadPath() {
        return Environment.DIRECTORY_PICTURES + File.separator + getAppName();
    }

    public String getAppName() {
        String appId = BuildConfig.APPLICATION_ID;
        String[] myArray = appId.split("\\.");
        return myArray[myArray.length - 1];
    }

    public String getDateFromMillisecond(long millisecond) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        formatter.setTimeZone(TimeZone.getTimeZone("Turkey"));
        return formatter.format(new Date(millisecond));
    }

    public long getMillisecondFromDate(String date, String time) {
        long timeInMilliseconds = 0;
        String givenDateString = date + " " + time;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        try {
            Date mDate = sdf.parse(givenDateString);
            timeInMilliseconds = mDate.getTime();
            System.out.println("Date in milli :: " + timeInMilliseconds);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return timeInMilliseconds;
    }

    public long getMillisecondFromDate(String date) {
        long timeInMilliseconds = 0;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date mDate = sdf.parse(date);
            timeInMilliseconds = mDate.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return timeInMilliseconds;
    }


}