package com.actionmobile.asitakvimi.util;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import com.actionmobile.asitakvimi.model.Child;
import com.actionmobile.asitakvimi.model.Vaccine.TakvimBean;
import com.actionmobile.asitakvimi.otto.AddNotification;
import com.actionmobile.asitakvimi.otto.BusProvider;

public class NotificationPublisher extends BroadcastReceiver {

    public static final String CHANNEL_ID = "1";
    public static String NOTIFICATION_ID = "notification-id";
    public static String NOTIFICATION = "notification";
    public static String KEY_CHILD = "child";
    public static String KEY_BUNDLE = "key_bundle";
    public static String KEY_TAKVIM = "key_takvim";

    private Notification notification;
    private int id;
    private Child child;
    private TakvimBean takvim;

    @Override
    public void onReceive(Context context, Intent intent) {
        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        Bundle bundle = intent.getBundleExtra(KEY_BUNDLE);
        if (bundle != null) {
            notification = bundle.getParcelable(NOTIFICATION);
            id = bundle.getInt(NOTIFICATION_ID, 0);
            child = (Child) bundle.getSerializable(KEY_CHILD);
            takvim = (TakvimBean) bundle.getSerializable(KEY_TAKVIM);
        }

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    "Bildirim Kanalı",
                    NotificationManager.IMPORTANCE_DEFAULT);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
        }

        if (notificationManager != null) {
            notificationManager.notify(id, notification);
        }

        BusProvider.getInstance().post(new AddNotification(child, takvim));
    }
}
