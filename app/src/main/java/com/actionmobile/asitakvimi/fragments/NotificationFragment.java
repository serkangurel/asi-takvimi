package com.actionmobile.asitakvimi.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.actionmobile.asitakvimi.R;
import com.actionmobile.asitakvimi.adapter.NotificationAdapter;
import com.actionmobile.asitakvimi.model.Notifications;
import com.actionmobile.asitakvimi.model.Vaccine.AsilarBean;
import com.actionmobile.asitakvimi.otto.AddNotification;
import com.actionmobile.asitakvimi.otto.UpdateNotificationList;
import com.actionmobile.asitakvimi.util.Constance;
import com.actionmobile.asitakvimi.util.Tools;
import com.orhanobut.hawk.Hawk;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.HashMap;

public class NotificationFragment extends BaseFragment {

    private String mParam2;

    private HashMap<Integer,AsilarBean> asilarHashMap = new HashMap<>();
    private NotificationAdapter adapter;
    private TextView tvInstruction;
    private RecyclerView rvNotification;


    public NotificationFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static NotificationFragment newInstance(HashMap<Integer,AsilarBean> param1, String param2) {
        NotificationFragment fragment = new NotificationFragment();
        Bundle args = new Bundle();
        args.putSerializable(Constance.ARG_PARAM1, param1);
        args.putString(Constance.ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            asilarHashMap = (HashMap<Integer,AsilarBean>) getArguments().getSerializable(Constance.ARG_PARAM1);
            mParam2 = getArguments().getString(Constance.ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_notification, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvInstruction = view.findViewById(R.id.tv_instruction);
        rvNotification = view.findViewById(R.id.rv_notification);

        ArrayList<Notifications> notificationList = Hawk.get(Constance.HAWK_NOTIFICATIONS, new ArrayList<>());
        updateUI(notificationList);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvNotification.setLayoutManager(layoutManager);

        adapter = new NotificationAdapter(getContext(), asilarHashMap);
        rvNotification.setAdapter(adapter);
        adapter.setList(notificationList);

    }

    private void updateUI(ArrayList<Notifications> notificationList) {
        if (notificationList.size() == 0) {
            rvNotification.setVisibility(View.GONE);
            tvInstruction.setVisibility(View.VISIBLE);
        } else {
            rvNotification.setVisibility(View.VISIBLE);
            tvInstruction.setVisibility(View.GONE);
        }
    }

    @Subscribe
    public void AddNotification(AddNotification event) {
        ArrayList<Notifications> notificationList = Hawk.get(Constance.HAWK_NOTIFICATIONS, new ArrayList<>());
        Notifications notification = new Notifications();
        notification.id = event.getTakvim().id;
        notification.child = event.getChild();
        notification.takvim = event.getTakvim();
        String vaccineName = asilarHashMap.get(event.getTakvim().AsiId).KisaAdi;
        long date = Tools.getInstance().getMillisecondFromDate(event.getTakvim().Tarih);
        if (!DateUtils.isToday(date)) {
            notification.message = new StringBuilder()
                    .append("Geçmiş tarihli bir aşınız var: ")
                    .append(vaccineName)
                    .append(" - ")
                    .append(event.getTakvim().Tarih).toString();
        } else {
            notification.message = new StringBuilder()
                    .append("Aşı zamanı geldi: ")
                    .append(vaccineName)
                    .append(" - ")
                    .append(event.getTakvim().Tarih).toString();
        }
        notificationList.add(notification);
        Hawk.put(Constance.HAWK_NOTIFICATIONS, notificationList);
        updateUI(notificationList);
        adapter.setList(notificationList);
    }

    @Subscribe
    public void updateNotificationList(UpdateNotificationList event) {
        ArrayList<Notifications> notificationList = Hawk.get(Constance.HAWK_NOTIFICATIONS, new ArrayList<>());
        updateUI(notificationList);
        adapter.setList(notificationList);
    }
}
