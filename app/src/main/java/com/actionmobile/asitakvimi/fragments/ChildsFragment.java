package com.actionmobile.asitakvimi.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.actionmobile.asitakvimi.R;
import com.actionmobile.asitakvimi.activity.AddChildActivity;
import com.actionmobile.asitakvimi.adapter.ChildAdapter;
import com.actionmobile.asitakvimi.model.Child;
import com.actionmobile.asitakvimi.model.Vaccine.AsilarBean;
import com.actionmobile.asitakvimi.model.Vaccine.TakvimBean;
import com.actionmobile.asitakvimi.util.Constance;
import com.orhanobut.hawk.Hawk;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class ChildsFragment extends Fragment {

    private ChildAdapter adapter;

    private TextView tvInstruction;
    private RecyclerView rvChild;

    private HashMap<Integer,AsilarBean> asilarHashMap = new HashMap<>();
    private ArrayList<TakvimBean> takvimList = new ArrayList<>();

    public ChildsFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static ChildsFragment newInstance(ArrayList<TakvimBean> param1, HashMap<Integer,AsilarBean> param2) {
        ChildsFragment fragment = new ChildsFragment();
        Bundle args = new Bundle();
        args.putSerializable(Constance.ARG_PARAM1, param1);
        args.putSerializable(Constance.ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            takvimList = (ArrayList<TakvimBean>) getArguments().getSerializable(Constance.ARG_PARAM1);
            asilarHashMap = (HashMap<Integer,AsilarBean>) getArguments().getSerializable(Constance.ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_childs, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        FloatingActionButton fab = view.findViewById(R.id.fab);
        tvInstruction = view.findViewById(R.id.tv_instruction);
        rvChild = view.findViewById(R.id.rv_child);
        HashMap<String,Child> childHashMap = Hawk.get(Constance.HAWK_CHILDS, new HashMap<>());
        updateUI(childHashMap);

        List<Child> childArrayList = new ArrayList<>(childHashMap.values());
        Collections.sort(childArrayList, (m1, m2) -> Long.compare(m1.creationDate, m2.creationDate));


        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvChild.setLayoutManager(layoutManager);

        adapter = new ChildAdapter(getContext(), asilarHashMap);
        rvChild.setAdapter(adapter);
        adapter.setList(childArrayList);

        fab.setOnClickListener(view1 -> startAddChildActivity());

    }

    private void updateUI(HashMap<String,Child> childList) {
        if (childList.size() == 0) {
            rvChild.setVisibility(View.GONE);
            tvInstruction.setVisibility(View.VISIBLE);
        } else {
            rvChild.setVisibility(View.VISIBLE);
            tvInstruction.setVisibility(View.GONE);
        }
    }

    private void startAddChildActivity() {
        Intent intent = new Intent(getContext(), AddChildActivity.class);
        intent.putExtra(Constance.KEY_TAKVIM, takvimList);
        intent.putExtra(Constance.KEY_ASI, asilarHashMap);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        HashMap<String,Child> childHashMap = Hawk.get(Constance.HAWK_CHILDS, new HashMap<>());
        updateUI(childHashMap);
        List<Child> childArrayList = new ArrayList<>(childHashMap.values());
        Collections.sort(childArrayList, (m1, m2) -> Long.compare(m1.creationDate, m2.creationDate));
        adapter.setList(childArrayList);
    }
}
