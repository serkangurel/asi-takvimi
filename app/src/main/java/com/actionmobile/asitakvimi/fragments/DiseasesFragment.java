package com.actionmobile.asitakvimi.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.actionmobile.asitakvimi.R;
import com.actionmobile.asitakvimi.adapter.DiseaseAdapter;
import com.actionmobile.asitakvimi.model.Vaccine.HastaliklarBean;
import com.actionmobile.asitakvimi.util.Constance;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DiseasesFragment extends Fragment {

    private List<HastaliklarBean> diseaseList = new ArrayList<>();
    private String mParam2;


    public DiseasesFragment() {
        // Required empty public constructor
    }

    public static DiseasesFragment newInstance(ArrayList<HastaliklarBean> param1, String param2) {
        DiseasesFragment fragment = new DiseasesFragment();
        Bundle args = new Bundle();
        args.putSerializable(Constance.ARG_PARAM1, param1);
        args.putString(Constance.ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            diseaseList = (List<HastaliklarBean>) getArguments().getSerializable(Constance.ARG_PARAM1);
            mParam2 = getArguments().getString(Constance.ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_diseases, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView rvDiseases = view.findViewById(R.id.rv_diseases);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvDiseases.setLayoutManager(layoutManager);

        DiseaseAdapter adapter = new DiseaseAdapter(getContext());
        rvDiseases.setAdapter(adapter);
        adapter.setList(diseaseList);
    }

}
