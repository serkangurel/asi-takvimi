package com.actionmobile.asitakvimi.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.actionmobile.asitakvimi.R;
import com.actionmobile.asitakvimi.util.Constance;

public class AboutVaccineFragment extends Fragment {

    private String mParam1;
    private String mParam2;

    public AboutVaccineFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static AboutVaccineFragment newInstance(String param1, String param2) {
        AboutVaccineFragment fragment = new AboutVaccineFragment();
        Bundle args = new Bundle();
        args.putString(Constance.ARG_PARAM1, param1);
        args.putString(Constance.ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(Constance.ARG_PARAM1);
            mParam2 = getArguments().getString(Constance.ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_about_vaccine, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView tvAboutVaccine = view.findViewById(R.id.tv_about_vaccine);
        tvAboutVaccine.setText(Html.fromHtml(mParam1.replace("\n", "<br />")));
    }
}
