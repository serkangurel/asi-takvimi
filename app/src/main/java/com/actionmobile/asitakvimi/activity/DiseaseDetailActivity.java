package com.actionmobile.asitakvimi.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionmobile.asitakvimi.R;
import com.actionmobile.asitakvimi.model.Vaccine.HastaliklarBean;
import com.actionmobile.asitakvimi.util.Constance;

public class DiseaseDetailActivity extends BaseActivity {

    HastaliklarBean disease;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_disease_detail);
        TextView tvDiseaseDetail = findViewById(R.id.tv_disease_detail);
        TextView tvDiseaseTitle = findViewById(R.id.tv_title_disease);
        ImageView imgBack = findViewById(R.id.img_back_disease);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            disease = (HastaliklarBean) bundle.getSerializable(Constance.KEY_ACTIVITY);
        }
        imgBack.setOnClickListener(view -> onBackPressed());
        tvDiseaseTitle.setText(disease.HastalikAdi);
        tvDiseaseDetail.setText(Html.fromHtml(disease.Aciklama.replace("\n", "<br />")));
    }
}