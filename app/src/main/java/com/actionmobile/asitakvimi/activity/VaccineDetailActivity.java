package com.actionmobile.asitakvimi.activity;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionmobile.asitakvimi.R;
import com.actionmobile.asitakvimi.model.Child;
import com.actionmobile.asitakvimi.model.Notifications;
import com.actionmobile.asitakvimi.model.Vaccine.AsilarBean;
import com.actionmobile.asitakvimi.model.Vaccine.TakvimBean;
import com.actionmobile.asitakvimi.otto.BusProvider;
import com.actionmobile.asitakvimi.otto.UpdateNotificationList;
import com.actionmobile.asitakvimi.util.Constance;
import com.actionmobile.asitakvimi.util.Tools;
import com.bumptech.glide.Glide;
import com.orhanobut.hawk.Hawk;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;

public class VaccineDetailActivity extends BaseActivity {

    private TakvimBean takvim;
    private AsilarBean asi;
    private String childId;
    private CircleImageView imgProfile;
    private TextView name;
    private TextView vaccineShortName;
    private TextView date;
    private SwitchCompat status;
    private TextView vaccineLongName;
    private TextView aboutVaccine;
    private ImageView imgBack;
    private TextView title;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vaccine_detail);
        imgProfile = findViewById(R.id.img_child2);
        name = findViewById(R.id.tv_name2);
        vaccineShortName = findViewById(R.id.tv_vaccine_short_name);
        date = findViewById(R.id.tv_vaccine_date2);
        status = findViewById(R.id.status);
        vaccineLongName = findViewById(R.id.tv_vaccine_long_name);
        aboutVaccine = findViewById(R.id.tv_about_vaccine2);
        imgBack = findViewById(R.id.img_back_vaccine_detail);
        title = findViewById(R.id.tv_vaccine_title);

        imgBack.setOnClickListener(view -> onBackPressed());

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            childId = (String) bundle.getSerializable(Constance.KEY_CHILD);
            takvim = (TakvimBean) bundle.getSerializable(Constance.KEY_TAKVIM);
            asi = (AsilarBean) bundle.getSerializable(Constance.KEY_ASI);
        }
        HashMap<String,Child> childList = Hawk.get(Constance.HAWK_CHILDS, new HashMap<>());
        Child child = childList.get(childId);
        setChildPhoto(child);

        title.setText(asi.KisaAdi);
        name.setText(child.name);
        date.setText(takvim.Tarih);
        vaccineShortName.setText(asi.KisaAdi);
        vaccineLongName.setText(asi.TamAdi);
        aboutVaccine.setText(asi.Aciklama);

        if (takvim.Durum) {
            status.setChecked(true);
            status.setText("Yapıldı");
        } else {
            status.setChecked(false);
            status.setText("Yapılmadı");
        }

        boolean isDateFromPast = Tools.getInstance().isDateFromPast(takvim.Tarih);
        if (isDateFromPast) {
            status.setEnabled(true);
        } else {
            status.setEnabled(false);
        }

        status.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                compoundButton.setText("Yapıldı");

            } else {
                compoundButton.setText("Yapılmadı");
            }
        });

    }

    private void setVaccineStatus(boolean status) {
        HashMap<String,Child> childHashMap = Hawk.get(Constance.HAWK_CHILDS, new HashMap<>());
        Child c = childHashMap.get(childId);
        for (TakvimBean takvimBean : c.takvimList) {
            if (takvimBean.id.equals(takvim.id)) {
                takvimBean.Durum = status;
            }
        }
        Hawk.put(Constance.HAWK_CHILDS, childHashMap);
    }

    private void setNotificationStatus(boolean status) {
        Notifications mNotification = null;
        //Güncel notification listesi
        ArrayList<Notifications> notificationList = Hawk.get(Constance.HAWK_NOTIFICATIONS, new ArrayList<>());

        //Silinmiş notification listesi
        HashMap<String,Notifications> removedNotifications = Hawk.get(Constance.HAWK_REMOVED_NOTIFICATIONS, new HashMap<>());

        for (int i = 0; i < notificationList.size(); i++) {
            Notifications notification = notificationList.get(i);
            if (notification.id.equals(takvim.id)) {
                mNotification = notification;
                if (status) {
                    notificationList.remove(i);
                    removedNotifications.put(takvim.id, notification);
                }
                break;
            }
        }

        if (mNotification == null) {  //Notification önceden silinmiş ise
            Notifications removedNotification = removedNotifications.get(takvim.id);
            if (!status) {
                notificationList.add(removedNotification);
                removedNotifications.remove(takvim.id);
            }
        }
        Hawk.put(Constance.HAWK_NOTIFICATIONS, notificationList);
        Hawk.put(Constance.HAWK_REMOVED_NOTIFICATIONS, removedNotifications);
        BusProvider.getInstance().post(new UpdateNotificationList());

    }


    private void setChildPhoto(Child child) {
        if (child.picturePath != null) {    //Çocuğun fotoğrafı var
            File imageFile = new File(child.picturePath);
            if (imageFile.exists()) {   //fotoğraf verilen yoldan silinmemiş
                Bitmap compressedBitmap = null;
                try {
                    compressedBitmap = new Compressor(this)
                            .setMaxWidth(150)
                            .setMaxHeight(150)
                            .compressToBitmap(imageFile);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (compressedBitmap != null) {
                    Glide.with(this)
                            .load(compressedBitmap)
                            .into(imgProfile);
                }

            } else {    //fotoğraf verilen yoldan silinmiş
                imgProfile.setImageResource(R.drawable.ic_child);
            }

        } else {    //Çocuğun fotoğrafı yok
            imgProfile.setImageResource(R.drawable.ic_child);
        }
    }


    @Override
    protected void onPause() {
        if (status.isEnabled()) {
            setVaccineStatus(status.isChecked());
            setNotificationStatus(status.isChecked());
        }
        super.onPause();
    }

}