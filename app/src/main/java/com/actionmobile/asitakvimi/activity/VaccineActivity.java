package com.actionmobile.asitakvimi.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionmobile.asitakvimi.R;
import com.actionmobile.asitakvimi.adapter.VaccineAdapter;
import com.actionmobile.asitakvimi.model.Child;
import com.actionmobile.asitakvimi.model.Notifications;
import com.actionmobile.asitakvimi.model.Vaccine.AsilarBean;
import com.actionmobile.asitakvimi.model.Vaccine.TakvimBean;
import com.actionmobile.asitakvimi.otto.BusProvider;
import com.actionmobile.asitakvimi.otto.UpdateNotificationList;
import com.actionmobile.asitakvimi.util.Constance;
import com.actionmobile.asitakvimi.util.Tools;
import com.orhanobut.hawk.Hawk;

import java.util.ArrayList;
import java.util.HashMap;

public class VaccineActivity extends BaseActivity {
    private String childId;
    private VaccineAdapter adapter;
    private HashMap<Integer,AsilarBean> asilarHashMap;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vaccine);
        RecyclerView rvVaccine = findViewById(R.id.rv_vaccine);
        ImageView imgBack = findViewById(R.id.img_back_vaccine);
        TextView tvChildName = findViewById(R.id.tv_child_name);
        ImageView imgDeleteChild = findViewById(R.id.img_delete_child);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            childId = (String) bundle.getSerializable(Constance.KEY_ACTIVITY);
            asilarHashMap = (HashMap<Integer,AsilarBean>) bundle.getSerializable(Constance.KEY_ASI);
        }

        HashMap<String,Child> childHashMap = Hawk.get(Constance.HAWK_CHILDS, new HashMap<>());
        Child child = childHashMap.get(childId);

        tvChildName.setText(child.name);
        imgBack.setOnClickListener(view -> onBackPressed());

        imgDeleteChild.setOnClickListener(view -> {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("Çocuk Sil");
            alert.setMessage("Silmek istediğinize emin misiniz?");
            alert.setPositiveButton("Evet", (dialog, which) -> {
                // continue with delete
                cancelNotifications(child);
                childHashMap.remove(childId);
                Hawk.put(Constance.HAWK_CHILDS, childHashMap);
                dialog.dismiss();
                onBackPressed();
            });
            alert.setNegativeButton("Hayır", (dialog, which) -> {
                // close dialog
                dialog.dismiss();
            });
            alert.show();
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(VaccineActivity.this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvVaccine.setLayoutManager(layoutManager);

        adapter = new VaccineAdapter(VaccineActivity.this, asilarHashMap, child);
        rvVaccine.setAdapter(adapter);
        adapter.setList(child.takvimList);

    }

    private void cancelNotifications(Child child) { // Çocuk silindiğinde alarmları iptal et ve bildirimleri sil.
        for (TakvimBean takvimBean : child.takvimList) {
            Tools.getInstance().cancelAlarm(VaccineActivity.this, takvimBean.BildirimId);
        }
        ArrayList<Notifications> notificationList = Hawk.get(Constance.HAWK_NOTIFICATIONS, new ArrayList<>());
        for (int i = notificationList.size() - 1; i >= 0; i--) {
            if (child.id.equals(notificationList.get(i).child.id)) {
                notificationList.remove(i);
            }
        }
        Hawk.put(Constance.HAWK_NOTIFICATIONS, notificationList);
        BusProvider.getInstance().post(new UpdateNotificationList());
    }

    @Override
    public void onResume() {
        super.onResume();
        HashMap<String,Child> childHashMap = Hawk.get(Constance.HAWK_CHILDS, new HashMap<>());
        ArrayList<TakvimBean> takvimList = childHashMap.get(childId).takvimList;
        if (takvimList != null) {
            adapter.setList(takvimList);
        }
    }


}
