package com.actionmobile.asitakvimi.activity;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionmobile.asitakvimi.R;
import com.actionmobile.asitakvimi.model.Child;
import com.actionmobile.asitakvimi.model.Vaccine.AsilarBean;
import com.actionmobile.asitakvimi.model.Vaccine.TakvimBean;
import com.actionmobile.asitakvimi.otto.AddNotification;
import com.actionmobile.asitakvimi.otto.BusProvider;
import com.actionmobile.asitakvimi.util.Constance;
import com.actionmobile.asitakvimi.util.Tools;
import com.bumptech.glide.Glide;
import com.orhanobut.hawk.Hawk;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.enums.EPickType;
import com.vansuita.pickimage.listeners.IPickResult;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.UUID;

import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;

public class AddChildActivity extends BaseActivity implements IPickResult {

    private TextView tvTitle;
    private ImageView imageBack;
    private EditText etName;
    private EditText etDate;
    private Button btnSave;
    private CircleImageView imgProfile;
    private TextView tvAddImage;
    private String picturePath;

    private ArrayList<TakvimBean> takvimList = new ArrayList<>();
    private HashMap<Integer,AsilarBean> asilarHashMap = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_child);

        etName = findViewById(R.id.et_name);
        etDate = findViewById(R.id.et_date);
        tvTitle = findViewById(R.id.tv_add_child);
        imageBack = findViewById(R.id.img_back);
        btnSave = findViewById(R.id.btn_save);
        imgProfile = findViewById(R.id.img_profile);
        tvAddImage = findViewById(R.id.tv_add_image);

        tvTitle.setText(R.string.add_child);

        etDate.setOnClickListener(dateClickListener);
        imageBack.setOnClickListener(view -> onBackPressed());
        btnSave.setOnClickListener(saveClickListener);

        imgProfile.setImageResource(R.drawable.ic_child);
        imgProfile.setOnClickListener(view -> addImage());
        tvAddImage.setOnClickListener(view -> addImage());


        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            takvimList = (ArrayList<TakvimBean>) bundle.getSerializable(Constance.KEY_TAKVIM);
            asilarHashMap = (HashMap<Integer,AsilarBean>) bundle.getSerializable(Constance.KEY_ASI);
        }
    }

    public String addMonths(String dateAsString, int nbMonths) {
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", new Locale("tr"));
        Date dateAsObj = null;
        try {
            dateAsObj = sdf.parse(dateAsString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(dateAsObj);
        cal.add(Calendar.MONTH, nbMonths);
        Date dateAsObjAfterAMonth = cal.getTime();
        return sdf.format(dateAsObjAfterAMonth);
    }


    private void addImage() {
        PickImageDialog.build(new PickSetup()
                .setTitle("Seç")
                .setPickTypes(EPickType.GALLERY, EPickType.CAMERA)
                .setProgressText("Yükleniyor")
                .setCameraButtonText("Kamera")
                .setGalleryButtonText("Galeri")
                .setTitleColor(getResources().getColor(R.color.black))
                .setCancelTextColor(getResources().getColor(R.color.black))
                .setCancelText("İptal")
                .setCameraIcon(R.drawable.ic_camera)
                .setGalleryIcon(R.drawable.ic_gallery))
                .show(AddChildActivity.this);
    }

    @Override
    public void onPickResult(PickResult pickResult) {
        if (pickResult.getError() == null) {
            picturePath = pickResult.getPath();
            File imageFile = new File(picturePath);
            Bitmap compressedBitmap = null;
            try {
                compressedBitmap = new Compressor(this)
                        .setMaxWidth(150)
                        .setMaxHeight(150)
                        .compressToBitmap(imageFile);
            } catch (IOException e) {
                e.printStackTrace();
            }

            Glide.with(this)
                    .load(compressedBitmap)
                    .into(imgProfile);

        } else {
            //Handle possible errors
            //TODO: do what you have to do with pickResult.getError();
            Toast.makeText(this, pickResult.getError().getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    View.OnClickListener saveClickListener = view -> {
        if (!validate()) {
            saveFailed();
            return;
        }

        String name = etName.getText().toString();
        String date = etDate.getText().toString();
        String randomId = UUID.randomUUID().toString();

        HashMap<String,Child> childList = Hawk.get(Constance.HAWK_CHILDS, new HashMap<>());

        Child child = new Child();
        child.creationDate = System.currentTimeMillis();
        child.id = randomId;
        child.picturePath = picturePath;
        child.name = name;
        child.birthDate = date;
        child.takvimList = new ArrayList<>();

        for (int i = 0; i < takvimList.size(); i++) {
            takvimList.get(i).id = UUID.randomUUID().toString();
            takvimList.get(i).Tarih = addMonths(child.birthDate, takvimList.get(i).XAySonra);
            int bildirimId = (int) UUID.randomUUID().getLeastSignificantBits();
            takvimList.get(i).BildirimId = Math.abs(bildirimId);
        }
        child.takvimList = takvimList;
        setNotifications(child);
        childList.put(randomId, child);
        Hawk.put(Constance.HAWK_CHILDS, childList);
        onBackPressed();
    };

    private void setNotifications(Child child) {
        long currentTimeMillis = System.currentTimeMillis();
        for (TakvimBean takvimBean : child.takvimList) {
            String vaccineName = asilarHashMap.get(takvimBean.AsiId).KisaAdi;
            long date = Tools.getInstance().getMillisecondFromDate(takvimBean.Tarih);
            boolean isToday = DateUtils.isToday(date);

            if (isToday) { //Bugün tarihi gelen aşı için alarm
                Tools.getInstance()
                        .setNotification(AddChildActivity.this,
                                takvimBean, child, vaccineName, currentTimeMillis);

            } else if (currentTimeMillis - date > 0) {  //Geçmiş tarihli aşılar için bildirim
                BusProvider.getInstance().post(new AddNotification(child, takvimBean));

            } else { //Gelecek tarihli aşılar için alarm
                Tools.getInstance()
                        .setNotification(AddChildActivity.this,
                                takvimBean, child, vaccineName, date);
            }
        }
    }

    View.OnClickListener dateClickListener = view -> {
        // Şimdiki zaman bilgilerini alıyoruz. güncel yıl, güncel ay, güncel gün.
        final Calendar takvim = Calendar.getInstance();
        int yil = takvim.get(Calendar.YEAR);
        int ay = takvim.get(Calendar.MONTH);
        int gun = takvim.get(Calendar.DAY_OF_MONTH);

        @SuppressLint("SetTextI18n")
        DatePickerDialog dpd = new DatePickerDialog(this, android.R.style.Theme_Holo_Light_Dialog,
                (view1, year, month, dayOfMonth) -> {
                    String dayOfMonthh;
                    String monthh;
                    String yearr;
                    // ay değeri 0 dan başladığı için (Ocak=0, Şubat=1,..,Aralık=11)
                    // değeri 1 artırarak gösteriyoruz.
                    month += 1;
                    if (dayOfMonth < 10) {
                        dayOfMonthh = "0" + String.valueOf(dayOfMonth);
                    } else {
                        dayOfMonthh = String.valueOf(dayOfMonth);
                    }

                    if (month < 10) {
                        monthh = "0" + String.valueOf(month);
                    } else {
                        monthh = String.valueOf(month);
                    }
                    yearr = String.valueOf(year);
                    // year, month ve dayOfMonth değerleri seçilen tarihin değerleridir.
                    // Edittextte bu değerleri gösteriyoruz.
                    etDate.setText(dayOfMonthh + "/" + monthh + "/" + yearr);
                }, yil, ay, gun);
        // datepicker açıldığında set edilecek değerleri buraya yazıyoruz.
        // şimdiki zamanı göstermesi için yukarda tanmladığımz değşkenleri kullanyoruz.

        // showDialog penceresinin button bilgilerini ayarlıyoruz ve ekranda gösteriyoruz.
        dpd.setButton(DatePickerDialog.BUTTON_POSITIVE, "Seç", dpd);
        dpd.setButton(DatePickerDialog.BUTTON_NEGATIVE, "İptal", dpd);
        dpd.setTitle("Doğum Tarihini Seçiniz");
        dpd.getDatePicker().setMaxDate(System.currentTimeMillis());
        dpd.show();
    };

    private void saveFailed() {
        Toast.makeText(this, "Kayıt Başarısız!", Toast.LENGTH_LONG).show();
    }

//    void showDialog() {
//        String[] array = getResources().getStringArray(R.array.gender);
//        AlertDialog.Builder alt_bld = new AlertDialog.Builder(this);
//        //alt_bld.setIcon(R.drawable.icon);
//        alt_bld.setTitle("Cinsiyeti Seçiniz");
//        alt_bld.setSingleChoiceItems(array, -1, (dialog, item) -> {
//            etGender.setText(array[item]);
//            dialog.dismiss();// dismiss the alertbox after chose option
//
//        });
//        AlertDialog alert = alt_bld.create();
//        alert.show();
//    }

    private boolean validate() {
        boolean valid = true;

        String name = etName.getText().toString();
        String date = etDate.getText().toString();

        if (name.length() < 3) {
            etName.setError("Minimum 3 karakter giriniz");
            valid = false;
        } else {
            etName.setError(null);
        }

        if (date.isEmpty()) {
            etDate.setError("Doğum tarihini seciniz");
            valid = false;
        } else {
            etDate.setError(null);
        }

        return valid;
    }

}
