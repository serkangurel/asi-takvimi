package com.actionmobile.asitakvimi.otto;

import com.actionmobile.asitakvimi.model.Child;
import com.actionmobile.asitakvimi.model.Vaccine.TakvimBean;

public class AddNotification {

    private Child child;
    private TakvimBean takvim;

    public AddNotification(Child child, TakvimBean takvim) {
        this.child = child;
        this.takvim = takvim;
    }

    public Child getChild() {
        return child;
    }

    public TakvimBean getTakvim() {
        return takvim;
    }
}
