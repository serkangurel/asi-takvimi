package com.actionmobile.asitakvimi.model;

import java.io.Serializable;
import java.util.List;

public class Vaccine {

    public static class AsilarBean implements Serializable {
        public String TamAdi;
        public String Aciklama;
        public String KisaAdi;
        public int id;
    }

    public static class TakvimBean implements Serializable {
        public String id;
        public int AsiId;
        public int BildirimId;
        public boolean Durum;
        public int XAySonra;
        public String Tarih;
    }

    public static class HastaliklarBean implements Serializable {
        public String HastalikAdi;
        public String Aciklama;
    }

}