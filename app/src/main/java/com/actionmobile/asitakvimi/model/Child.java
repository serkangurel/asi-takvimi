package com.actionmobile.asitakvimi.model;


import com.actionmobile.asitakvimi.model.Vaccine.TakvimBean;

import java.io.Serializable;
import java.util.ArrayList;

public class Child implements Serializable {
    public String id;
    public long creationDate;
    public String picturePath;
    public String name;
    public String birthDate;
    public ArrayList<TakvimBean> takvimList;

}