package com.actionmobile.asitakvimi.model;

import com.actionmobile.asitakvimi.model.Vaccine.TakvimBean;
import java.io.Serializable;

public class Notifications implements Serializable {
    public String id;
    public Child child;
    public TakvimBean takvim;
    public String message;
}