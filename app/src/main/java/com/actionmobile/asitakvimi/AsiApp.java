package com.actionmobile.asitakvimi;

import android.app.Application;
import android.content.Context;

import com.orhanobut.hawk.Hawk;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class AsiApp extends Application {
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Roboto-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
        Hawk.init(this).build();
        context = this;
    }

    public static Context getContext() {
        return context;
    }
}
